@extends('adminlte.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>List Pertanyaan</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">Home</a></li>
          <li class="breadcrumb-item active">Forum Diskusi</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<div class="card ml-3">
  <div class="card-header">
    <h3 class="card-title">Pertanyaan</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
  	@if(session('success'))
  		<div class="alert alert-success">
  			{{session('success')}}
  		</div>
  	@endif
  	<a href="/pertanyaan/create" class="btn btn-primary mb-3">Buat Pertanyaan</a>
    <table class="table table-bordered">
      <thead>                  
        <tr>
          <th style="width: 10px">#</th>
          <th>Judul</th>
          <th>Pertanyaan</th>
          <th style="width: 40px">Action</th>
        </tr>
      </thead>
      <tbody>
      	@forelse($pertanyaan as $key=>$post)
      	<tr>
          <td>{{ $key+1 }}</td>
          <td>{{ $post->judul }}</td>
          <td>{{ $post->isi }}</td>
          <td style="display: flex">
          	<a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">show</a>
          	<a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-warning btn-sm ml-2">edit</a>
          	<form action="/pertanyaan/{{$post->id}}" method="POST">
          		@csrf
          		@method('DELETE')
          		<input type="submit" value="delete" class="btn btn-danger btn-sm ml-2">
	        </form>
          </td>
        </tr>
        @empty
        <tr>
        	<td colspan="4" align="center">No Data</td>
        </tr>
      	@endforelse
      </tbody>
    </table>
  </div>
</div>
@endsection