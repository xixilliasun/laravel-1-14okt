<!DOCTYPE html>
<html>
<head>
	<title>Buat Akun Baru</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="welcome" method="post">
		@csrf
		<label>First name :</label><br><br>
		<input type="text" name="fname"><br><br>
		<label>Last name :</label><br><br>
		<input type="text" name="lname"><br><br>
		<label>Gender :</label><br><br>
		<input type="radio" name="gender" value="Male">Male<br>
		<input type="radio" name="gender" value="Female">Female<br>
		<input type="radio" name="gender" value="Other">Other<br><br>
		<label>Nationality :</label><br><br>
		<select name="nation">
			<option value="Indonesian">Indonesian</option>
			<option value="Malaysian">Malaysian</option>
			<option value="Canadian">Canadian</option>
		</select><br><br>
		<label>Language Spoken :</label><br><br>
		<input type="checkbox" name="lang" value="Indonesia">Bahasa Indonesia<br>
		<input type="checkbox" name="lang" value="English">English<br>
		<input type="checkbox" name="lang" value="Other">Other<br><br>
		<label>Bio : </label><br><br>
		<textarea name="bio" rows="10" cols="40"></textarea><br>
		<input type="submit" name="submit" value="Sign Up">
	</form>
</body>
</html>