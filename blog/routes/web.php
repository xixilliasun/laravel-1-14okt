<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('index');
// });
//Route::get('/', 'HomeController@Home');
Route::get('/register','AuthController@Register');
Route::get('/master', function(){
	return view('adminlte.master');
});
Route::get('/', function(){
	return view('adminlte.items.table1');
});
Route::get('/data-tables', function(){
	return view('adminlte.items.table2');
});
Route::post('/welcome','AuthController@Welcome');
//controller 17Okt
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');